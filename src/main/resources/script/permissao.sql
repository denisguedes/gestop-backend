-------------TABELA PERMISSÃO

-------CADASTRO
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (1, current_date, 'Menu - cadastrar', 'CADASTRO_MENU');

--------DASHBOARD
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (2, current_date, 'Paínel - menu', 'PAINEL_MENU');

--------SEGURANÇA
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (3, current_date, 'Segurança - menu', 'SEGURANCA_MENU');

--------ADMINISTRADOR
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (4, current_date, 'Administrador - menu', 'ADMINISTRADOR_MENU');

--------FINANCEIRO
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (5, current_date, 'Financeiro - menu', 'FINANCEIRO_MENU');

--------RELATORIO
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (6, current_date, 'Relatório - menu', 'RELATORIO_MENU');


---------PERFIL
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (7, current_date, 'Perfil - consultar', 'PERFIL_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (8, current_date, 'Perfil - cadastrar', 'PERFIL_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (9, current_date, 'Perfil - alterar', 'PERFIL_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (10, current_date, 'Perfil - excluir', 'PERFIL_EXCLUIR');

---------PERMISSAO
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (11, current_date, 'Permissão - consultar', 'PERMISSAO_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (12, current_date, 'Permissão - casdastrar', 'PERMISSAO_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (13, current_date, 'Permissão - alterar', 'PERMISSAO_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (14, current_date, 'Permissão - excluir', 'PERMISSAO_EXCLUIR');

---------USUÁRIO
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (15, current_date, 'Usuário - consultar', 'USUARIO_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (16, current_date, 'Usuário - cadastrar', 'USUARIO_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (17, current_date, 'Usuário - alterar', 'USUARIO_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (18, current_date, 'Usuário - excluir', 'USUARIO_EXCLUIR');

---------CIDADE
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (19, current_date, 'Cidade - consultar', 'CIDADE_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (20, current_date, 'Cidade - cadastrar', 'CIDADE_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (21, current_date, 'Cidade - alterar', 'CIDADE_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (22, current_date, 'Cidade - excluir', 'CIDADE_EXCLUIR');

---------PAÍS
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (23, current_date, 'País - consultar', 'PAIS_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (24, current_date, 'País - cadastrar', 'PAIS_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (25, current_date, 'País - alterar', 'PAIS_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (26, current_date, 'País - excluir', 'PAIS_EXCLUIR');

---------ESTADO
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (27, current_date, 'Estado - consultar', 'ESTADO_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (28, current_date, 'Estado - cadastrar', 'ESTADO_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (29, current_date, 'Estado - alterar', 'ESTADO_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (30, current_date, 'Estado - excluir', 'ESTADO_EXCLUIR');


---------CLIENTE
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (31, current_date, 'Cliente - consultar', 'CLIENTE_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (32, current_date, 'Cliente - cadastrar', 'CLIENTE_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (33, current_date, 'Cliente - alterar', 'CLIENTE_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (34, current_date, 'Cliente - excluir', 'CLIENTE_EXCLUIR');


---------FORNECEDOR
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (35, current_date, 'Fornecedor - consultar', 'FORNECEDOR_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (36, current_date, 'Fornecedor - cadastrar', 'FORNECEDOR_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (37, current_date, 'Fornecedor - alterar', 'FORNECEDOR_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (38, current_date, 'Fornecedor - excluir', 'FORNECEDOR_EXCLUIR');


---------PRODUTO
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (39, current_date, 'Produto - consultar', 'PRODUTO_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (40, current_date, 'Produto - cadastrar', 'PRODUTO_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (41, current_date, 'Produto - alterar', 'PRODUTO_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (42, current_date, 'Produto - excluir', 'PRODUTO_EXCLUIR');

---------CATEGORIA
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (43, current_date, 'Categoria - consultar', 'CATEGORIA_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (44, current_date, 'Categoria - cadastrar', 'CATEGORIA_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (45, current_date, 'Categoria - alterar', 'CATEGORIA_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (46, current_date, 'Categoria - excluir', 'CATEGORIA_EXCLUIR');

---------PEDIDO
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (47, current_date, 'Pedido - consultar', 'PEDIDO_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (48, current_date, 'Pedido - cadastrar', 'PEDIDO_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (49, current_date, 'Pedido - alterar', 'PEDIDO_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (50, current_date, 'Pedido - excluir', 'PEDIDO_EXCLUIR');

---------PAGAMENTO
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (51, current_date, 'Pagamento - consultar', 'PAGAMENTO_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (52, current_date, 'Pagamento - cadastrar', 'PAGAMENTO_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (53, current_date, 'Pagamento - alterar', 'PAGAMENTO_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (54, current_date, 'Pagamento - excluir', 'PAGAMENTO_EXCLUIR');

---------VENDA
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (55, current_date, 'Venda - consultar', 'VENDA_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (56, current_date, 'Venda - cadastrar', 'VENDA_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (57, current_date, 'Venda - alterar', 'VENDA_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (58, current_date, 'Venda - excluir', 'VENDA_EXCLUIR');

---------FORMA PAGAMENTO
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (59, current_date, 'Forma de Pagamento - consultar', 'FORMA_PAGAMENTO_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (60, current_date, 'Forma de Pagamento - cadastrar', 'FORMA_PAGAMENTO_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (61, current_date, 'Forma de Pagamento - alterar', 'FORMA_PAGAMENTO_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (62, current_date, 'Forma de Pagamento - excluir', 'FORMA_PAGAMENTO_EXCLUIR');


---------RELATÓRIO
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (69, current_date, 'Relatório - contas a pagar', 'RELATORIO_CONTAS_PAGAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (70, current_date, 'Relatório - contas a receber', 'RELATORIO_CONTAS_RECEBER');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (71, current_date, 'Relatório - produto em estoque', 'RELATORIO_PRODUTO_ESTOQUE');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (72, current_date, 'Relatório - vendas por produto', 'RELATORIO_VENDA_PRODUTO');


---------CARGO
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (73, current_date, 'Cargo - consultar', 'CARGO_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (74, current_date, 'Cargo - cadastrar', 'CARGO_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (75, current_date, 'Cargo - alterar', 'CARGO_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (76, current_date, 'Cargo - excluir', 'CARGO_EXCLUIR');

---------FUNCIONÁRIO
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (77, current_date, 'Funcionário - consultar', 'FUNCIONARIO_CONSULTAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (78, current_date, 'Funcionário - cadastrar', 'FUNCIONARIO_CADASTRAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (79, current_date, 'Funcionário - alterar', 'FUNCIONARIO_ALTERAR');

insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (80, current_date, 'Funcionário - excluir', 'FUNCIONARIO_EXCLUIR');

--------COLABORADOR
insert into bdadm.tb_permissao (codigo, data_cadastro, descricao, "role")
values (81, current_date, 'Colaborador - menu', 'COLABORADOR_MENU');

