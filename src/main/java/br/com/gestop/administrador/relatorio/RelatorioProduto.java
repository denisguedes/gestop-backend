package br.com.gestop.administrador.relatorio;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gestop-api/categoria/relatorio")
public class RelatorioProduto {

	@GetMapping("/pdf")
	public void gerarPdf() {
		
	}
	
	@GetMapping("/excel")
	public void gerarExcel() {
		
	}
}
