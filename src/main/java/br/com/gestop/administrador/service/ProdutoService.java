package br.com.gestop.administrador.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.administrador.model.Produto;
import br.com.gestop.administrador.model.filter.ProdutoFilter;
import br.com.gestop.administrador.repository.ProdutoRepository;
import br.com.gestop.generic.service.GenericBaseService;

@Service
public class ProdutoService  extends GenericBaseService<ProdutoRepository, Produto, Long>{
	
	public Page<Produto> pequisar(ProdutoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
}
