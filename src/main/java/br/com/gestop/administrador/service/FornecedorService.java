package br.com.gestop.administrador.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.administrador.model.Fornecedor;
import br.com.gestop.administrador.model.filter.FornecedorFilter;
import br.com.gestop.administrador.repository.FornecedorRepository;
import br.com.gestop.generic.service.GenericBaseService;

@Service
public class FornecedorService  extends GenericBaseService<FornecedorRepository, Fornecedor, Long>{
	
	public Page<Fornecedor> pequisar(FornecedorFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
}
