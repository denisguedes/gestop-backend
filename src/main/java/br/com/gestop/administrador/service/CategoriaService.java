package br.com.gestop.administrador.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.administrador.model.Categoria;
import br.com.gestop.administrador.model.filter.CategoriaFilter;
import br.com.gestop.administrador.repository.CategoriaRepository;
import br.com.gestop.generic.service.GenericBaseService;

@Service
public class CategoriaService  extends GenericBaseService<CategoriaRepository, Categoria, Long>{
	
	public Page<Categoria> pequisar(CategoriaFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
}
