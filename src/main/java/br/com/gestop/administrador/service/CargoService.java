package br.com.gestop.administrador.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.administrador.model.Cargo;
import br.com.gestop.administrador.model.filter.CargoFilter;
import br.com.gestop.administrador.repository.CargoRepository;
import br.com.gestop.generic.service.GenericBaseService;

@Service
public class CargoService  extends GenericBaseService<CargoRepository, Cargo, Long>{
	
	public Page<Cargo> pequisar(CargoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
}
