package br.com.gestop.administrador.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.administrador.model.Funcionario;
import br.com.gestop.administrador.model.filter.FuncionarioFilter;
import br.com.gestop.administrador.repository.FuncionarioRepository;
import br.com.gestop.generic.service.GenericBaseService;

@Service
public class FuncionarioService  extends GenericBaseService<FuncionarioRepository, Funcionario, Long>{
	
	public Page<Funcionario> pequisar(FuncionarioFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
}
