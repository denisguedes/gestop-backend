package br.com.gestop.administrador.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.administrador.model.Cidade;
import br.com.gestop.administrador.model.filter.CidadeFilter;
import br.com.gestop.administrador.repository.CidadeRepository;
import br.com.gestop.generic.service.GenericBaseService;

@Service
public class CidadeService  extends GenericBaseService<CidadeRepository, Cidade, Long>{
	
	public Page<Cidade> pequisar(CidadeFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
}
