package br.com.gestop.administrador.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.administrador.model.Pais;
import br.com.gestop.administrador.model.filter.PaisFilter;
import br.com.gestop.administrador.repository.PaisRepository;
import br.com.gestop.generic.service.GenericBaseService;

@Service
public class PaisService  extends GenericBaseService<PaisRepository, Pais, Long>{
	
	public Page<Pais> pequisar(PaisFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
}
