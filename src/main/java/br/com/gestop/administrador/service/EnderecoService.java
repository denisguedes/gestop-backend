package br.com.gestop.administrador.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.administrador.model.Endereco;
import br.com.gestop.administrador.model.filter.EnderecoFilter;
import br.com.gestop.administrador.repository.EnderecoRepository;
import br.com.gestop.generic.service.GenericBaseService;

@Service
public class EnderecoService  extends GenericBaseService<EnderecoRepository, Endereco, Long>{
	
	public Page<Endereco> pequisar(EnderecoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
}
