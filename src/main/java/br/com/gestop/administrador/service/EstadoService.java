package br.com.gestop.administrador.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.administrador.model.Estado;
import br.com.gestop.administrador.model.filter.EstadoFilter;
import br.com.gestop.administrador.repository.EstadoRepository;
import br.com.gestop.generic.service.GenericBaseService;

@Service
public class EstadoService  extends GenericBaseService<EstadoRepository, Estado, Long>{
	
	public Page<Estado> pequisar(EstadoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
}
