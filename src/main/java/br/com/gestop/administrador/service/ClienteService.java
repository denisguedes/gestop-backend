package br.com.gestop.administrador.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.administrador.model.Cliente;
import br.com.gestop.administrador.model.filter.ClienteFilter;
import br.com.gestop.administrador.repository.ClienteRepository;
import br.com.gestop.generic.service.GenericBaseService;

@Service
public class ClienteService  extends GenericBaseService<ClienteRepository, Cliente, Long>{
	
	public Page<Cliente> pequisar(ClienteFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public Cliente buscarPorNome(String nome){
		return repositorio.findByNome(nome);
	}
}
