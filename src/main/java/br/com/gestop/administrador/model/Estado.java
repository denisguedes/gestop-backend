package br.com.gestop.administrador.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table(name = "tb_estado", schema = "bd_gestor")
public class Estado extends GenericBaseModel<Long> {

	@NotNull
	@Size(min = 3, max = 30)
	@Column(name = "nome")
	private String nome;

	@NotNull
	@Size(min = 2, max = 2)
	@Column(name = "sigla")
	private String sigla;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "codigo_pais")
	private Pais pais;

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(nome).append(" - ")
		  .append(sigla);
		  if(pais != null) {
		  sb.append(" - ").append(pais.getNome());
		  }
		return sb.toString();
	}
}
