package br.com.gestop.administrador.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CNPJ;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table(name = "tb_fornecedor", schema = "bd_gestor")
public class Fornecedor extends GenericBaseModel<Long>{
	
	@Column(name = "razao_social")
	private String razaoSocial;
	
	@NotNull
	@Column(name = "nome_fantasia")
	private String nomeFantasia;
	
	@CNPJ
	@NotNull
	@Column(name = "cnpj")
	private String cnpj;

	@JsonManagedReference("contatosFornecedor")
	@OneToMany(mappedBy = "fornecedor", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Contato> contatos;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "codigo_endereco")
	private Endereco endereco;

	public Fornecedor() {
		this.contatos = new ArrayList<>();
		this.endereco = new Endereco();
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public List<Contato> getContatos() {
		return contatos;
	}

	public void setContatos(List<Contato> contatos) {
		if (contatos != null) {
			this.contatos.clear();
			this.contatos.addAll(contatos);
		} else {
			this.contatos = contatos;
		}
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		if (endereco == null || endereco.getLogradouro() == null) {
			this.endereco = null;
		} else {
			this.endereco = endereco;
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(nomeFantasia).append(" - ").append(cnpj);
		return sb.toString();
	}
}
