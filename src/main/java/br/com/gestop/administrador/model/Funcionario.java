package br.com.gestop.administrador.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table(name = "tb_funcionario", schema = "bd_gestor")
public class Funcionario extends GenericBaseModel<Long> {

	@NotNull
	@Column(name = "nome")
	private String nome;

	@NotNull
	@Column(name = "cpf")
	private String cpf;
	
	@Column(name = "data_nascimento")
	private LocalDate dataNascimento;
	
	@ManyToOne
	@JoinColumn(name = "codigo_cargo")
	private Cargo cargo;
	
	@JsonManagedReference("contatosFuncionario")
	@OneToMany(mappedBy = "funcionario", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Contato> contatos;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "codigo_endereco")
	private Endereco endereco;
	
	public Funcionario() {
		this.cargo = new Cargo();
		this.endereco = new Endereco();
		this.contatos = new ArrayList<>();
	}
	
	public Funcionario(@NotNull String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public LocalDate getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

	public Cargo getCargo() {
		return cargo;
	}

	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}

	public List<Contato> getContatos() {
		return contatos;
	}

	public void setContatos(List<Contato> contatos) {
		if (contatos != null) {
			this.contatos.clear();
			this.contatos.addAll(contatos);
		} else {
			this.contatos = contatos;
		}
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		if (endereco == null || endereco.getLogradouro() == null) {
			this.endereco = null;
		} else {
			this.endereco = endereco;
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(nome).append(" - ").append(cpf);
		return sb.toString();
	}
}
