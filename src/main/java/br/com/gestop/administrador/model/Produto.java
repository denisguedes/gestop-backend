package br.com.gestop.administrador.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table(name = "tb_produto", schema = "bd_gestor")
public class Produto extends GenericBaseModel<Long>{

	@NotNull
	@Size(min = 1, max = 40)
	@Column(name = "descricao")
	private String descricao;
	
	@NotNull
	@Column(name = "codigo_proprio")
	private String codigoProprio;
	
	@Column(name = "arquivar")
	private Boolean arquivar;
	
	@Column(name = "valor_custo")
	private BigDecimal valorCusto;
	
	@Column(name = "valor_venda")
	private BigDecimal valorVenda;
	
	@NotNull
	@Column(name = "quantidade_atual")
	private Integer qtdAtual;
	
	@Column(name = "quantidade_minima")
	private Integer qtdMinima;
	
	@Column(name = "observacao")
	private String observacao;
	
	@Column(name = "valor_desconto")
	private BigDecimal valorDesconto;
	
	@Column(name = "valor_margem")
	private BigDecimal valorMargem;
	
	@Column(name = "embalagem")
	private String embalagem;
	
	@Column(name = "percentual")
	private BigDecimal percentual;
	
	@ManyToOne
	@JoinColumn(name = "codigo_fornecedor")
	private Fornecedor fornecedor;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "codigo_categoria")
	private Categoria categoria;
	
	public Produto() {}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCodigoProprio() {
		return codigoProprio;
	}

	public void setCodigoProprio(String codigoProprio) {
		this.codigoProprio = codigoProprio;
	}

	public Boolean getArquivar() {
		return arquivar;
	}

	public void setArquivar(Boolean arquivar) {
		this.arquivar = arquivar;
	}

	public BigDecimal getValorCusto() {
		return valorCusto;
	}

	public void setValorCusto(BigDecimal valorCusto) {
		this.valorCusto = valorCusto;
	}

	public BigDecimal getValorVenda() {
		return valorVenda;
	}

	public void setValorVenda(BigDecimal valorVenda) {
		this.valorVenda = valorVenda;
	}

	public Integer getQtdAtual() {
		return qtdAtual;
	}

	public void setQtdAtual(Integer qtdAtual) {
		this.qtdAtual = qtdAtual;
	}

	public Integer getQtdMinima() {
		return qtdMinima;
	}

	public void setQtdMinima(Integer qtdMinima) {
		this.qtdMinima = qtdMinima;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public BigDecimal getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(BigDecimal valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public BigDecimal getValorMargem() {
		return valorMargem;
	}

	public void setValorMargem(BigDecimal valorMargem) {
		this.valorMargem = valorMargem;
	}

	public String getEmbalagem() {
		return embalagem;
	}

	public void setEmbalagem(String embalagem) {
		this.embalagem = embalagem;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		if (fornecedor == null || fornecedor.getNomeFantasia() == null) {
			this.fornecedor = null;
		} else {
			this.fornecedor = fornecedor;
		}
	}

	public BigDecimal getPercentual() {
		return percentual;
	}

	public void setPercentual(BigDecimal percentual) {
		this.percentual = percentual;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(descricao).append(" - ")
		  .append(codigoProprio).append(" - ")
		  .append(valorVenda);
		return sb.toString();
	}
	
}
