package br.com.gestop.administrador.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table( name = "tb_categoria", schema = "bd_gestor")
public class Categoria extends GenericBaseModel<Long>{
	
	@NotNull
	@Size(min = 1, max = 30)
	@Column( name = "descricao")
	private String descricao;
	
	public Categoria() {}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(descricao);
		return sb.toString();
	}
}
