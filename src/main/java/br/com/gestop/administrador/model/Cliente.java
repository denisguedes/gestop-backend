package br.com.gestop.administrador.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table(name = "tb_cliente", schema = "bd_gestor")
public class Cliente extends GenericBaseModel<Long> {

	@Column(name = "razao_social")
	@NotNull
	private String razaoSocial;

	@Column(name = "nome")
	@NotNull
	private String nome;

	@Column(name = "cpf_cnpj")
	@NotNull
	private String cpfCnpj;

	@JsonManagedReference("contatos")
	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Contato> contatos;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "codigo_endereco")
	private Endereco endereco;
	
	public Cliente() {
		this.contatos = new ArrayList<>();
		this.endereco = new Endereco();
	}
	
	public Cliente(@NotNull String nome) {
		super();
		this.nome = nome;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public List<Contato> getContatos() {
		return contatos;
	}

	public void setContatos(List<Contato> contatos) {
		if (contatos != null) {
			this.contatos.clear();
			this.contatos.addAll(contatos);
		} else {
			this.contatos = contatos;
		}
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		if (endereco == null || endereco.getLogradouro() == null) {
			this.endereco = null;
		} else {
			this.endereco = endereco;
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(nome).append(" - ").append(cpfCnpj);
		return sb.toString();
	}
}
