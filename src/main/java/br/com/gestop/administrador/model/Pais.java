package br.com.gestop.administrador.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table(name = "tb_pais", schema = "bd_gestor")
public class Pais extends GenericBaseModel<Long> {

	@NotNull
	@Size(min = 3, max = 30)
	@Column(name = "nome")
	private String nome;
	
	public Pais() { }

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(nome);
		return sb.toString();
	}
}
