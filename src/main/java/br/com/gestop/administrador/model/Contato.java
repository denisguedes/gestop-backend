package br.com.gestop.administrador.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table(name = "tb_contato", schema = "bd_gestor")
public class Contato extends GenericBaseModel<Long>{

	@NotNull
	@Column( name = "telefone")
	private String telefone;
	
	@Size(min = 1, max = 40)
	@Email
	@Column( name = "email")
	private String email;
	
	@ManyToOne
	@JsonBackReference("contatos")
	@JoinColumn(name = "codigo_cliente")
	private Cliente cliente;
	
	@ManyToOne
	@JsonBackReference("contatosFornecedor")
	@JoinColumn(name = "codigo_fornecedor")
	private Fornecedor fornecedor;
	
	@ManyToOne
	@JsonBackReference("contatosFuncionario")
	@JoinColumn(name = "codigo_funcionario")
	private Funcionario funcionario;
	
	public Contato() {}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(email).append(" - ")
		  .append(telefone);
		return sb.toString();
	}
}
