package br.com.gestop.administrador.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table(name = "tb_cidade", schema = "bd_gestor")
public class Cidade extends GenericBaseModel<Long> {

	@NotNull
	@Size(min = 3, max = 30)
	@Column(name = "nome")
	private String nome;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "codigo_estado")
	private Estado estado;
	
	public Cidade() {}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(nome);
			if(estado != null) {
				sb.append(" - ").append(estado.getNome());
			}
		return sb.toString();
	}
}
