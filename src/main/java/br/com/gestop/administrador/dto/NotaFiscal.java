package br.com.gestop.administrador.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class NotaFiscal implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Date dataRecebimento;
	
	private String identificadorAssinatura;
	
	private String numeroNota;
	
	private String chaveAcesso;
	
	private String naturezaOperacao;
	
	private String dadoNota;
	
	private String inscricaoEstadual;
	
	private String inscricaoEstatualSub;
	
	private String cnpjCpf;
	
	private String razaoSocial;
	
	private Date dataEmissao;
	
	private String endereco;
	
	private String bairro;
	
	private String cep;
	
	private Date dataEntrada;
	
	private String municipio;
	
	private String fone;
	
	private String uf;
	
	private String inscricaoEstadualCidade;
	
	private String dataHora;
	
	private Integer quantidade;
	
	private String especie;
	
	private String marca;
	
	private String freteConta;
	
	private String codAntt;
	
	private String placaVeiculo;
	
	private String numeracao;
	
	private String pesoBruto;
	
	private String pesoLiquido;
	
	private BigDecimal valorBaseCalc;
	
	private BigDecimal valorIcms;
	
	private BigDecimal valorBaseIcms;
	
	private BigDecimal valorIcmsSub;
	
	private BigDecimal valorImportacao;
	
	private BigDecimal valorIcmsRemet;
	
	private BigDecimal valorFcp;
	
	private BigDecimal valorPis;
	
	private BigDecimal valorTotalProduto;
	
	private BigDecimal valorFrete;
	
	private BigDecimal valorSeguro;
	
	private BigDecimal valorDesconto;
	
	private BigDecimal valorOutrasDesp;
	
	private BigDecimal valorTotal;
	
	private BigDecimal valorIcmsDest;
	
	private BigDecimal valorTotalTrib;
	
	private BigDecimal valorCofins;
	
	private BigDecimal valorTotalNota;
	
	public NotaFiscal() {
		super();
	}

	public Date getDataRecebimento() {
		return dataRecebimento;
	}

	public void setDataRecebimento(Date dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}

	public String getIdentificadorAssinatura() {
		return identificadorAssinatura;
	}

	public void setIdentificadorAssinatura(String identificadorAssinatura) {
		this.identificadorAssinatura = identificadorAssinatura;
	}

	public String getNumeroNota() {
		return numeroNota;
	}

	public void setNumeroNota(String numeroNota) {
		this.numeroNota = numeroNota;
	}

	public String getChaveAcesso() {
		return chaveAcesso;
	}

	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}

	public String getNaturezaOperacao() {
		return naturezaOperacao;
	}

	public void setNaturezaOperacao(String naturezaOperacao) {
		this.naturezaOperacao = naturezaOperacao;
	}

	public String getDadoNota() {
		return dadoNota;
	}

	public void setDadoNota(String dadoNota) {
		this.dadoNota = dadoNota;
	}

	public String getInscricaoEstadual() {
		return inscricaoEstadual;
	}

	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}

	public String getInscricaoEstatualSub() {
		return inscricaoEstatualSub;
	}

	public void setInscricaoEstatualSub(String inscricaoEstatualSub) {
		this.inscricaoEstatualSub = inscricaoEstatualSub;
	}

	public String getCnpjCpf() {
		return cnpjCpf;
	}

	public void setCnpjCpf(String cnpjCpf) {
		this.cnpjCpf = cnpjCpf;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public Date getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(Date dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public Date getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(Date dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getFone() {
		return fone;
	}

	public void setFone(String fone) {
		this.fone = fone;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getInscricaoEstadualCidade() {
		return inscricaoEstadualCidade;
	}

	public void setInscricaoEstadualCidade(String inscricaoEstadualCidade) {
		this.inscricaoEstadualCidade = inscricaoEstadualCidade;
	}

	public String getDataHora() {
		return dataHora;
	}

	public void setDataHora(String dataHora) {
		this.dataHora = dataHora;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public String getEspecie() {
		return especie;
	}

	public void setEspecie(String especie) {
		this.especie = especie;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getFreteConta() {
		return freteConta;
	}

	public void setFreteConta(String freteConta) {
		this.freteConta = freteConta;
	}

	public String getCodAntt() {
		return codAntt;
	}

	public void setCodAntt(String codAntt) {
		this.codAntt = codAntt;
	}

	public String getPlacaVeiculo() {
		return placaVeiculo;
	}

	public void setPlacaVeiculo(String placaVeiculo) {
		this.placaVeiculo = placaVeiculo;
	}

	public String getNumeracao() {
		return numeracao;
	}

	public void setNumeracao(String numeracao) {
		this.numeracao = numeracao;
	}

	public String getPesoBruto() {
		return pesoBruto;
	}

	public void setPesoBruto(String pesoBruto) {
		this.pesoBruto = pesoBruto;
	}

	public String getPesoLiquido() {
		return pesoLiquido;
	}

	public void setPesoLiquido(String pesoLiquido) {
		this.pesoLiquido = pesoLiquido;
	}

	public BigDecimal getValorBaseCalc() {
		return valorBaseCalc;
	}

	public void setValorBaseCalc(BigDecimal valorBaseCalc) {
		this.valorBaseCalc = valorBaseCalc;
	}

	public BigDecimal getValorIcms() {
		return valorIcms;
	}

	public void setValorIcms(BigDecimal valorIcms) {
		this.valorIcms = valorIcms;
	}

	public BigDecimal getValorBaseIcms() {
		return valorBaseIcms;
	}

	public void setValorBaseIcms(BigDecimal valorBaseIcms) {
		this.valorBaseIcms = valorBaseIcms;
	}

	public BigDecimal getValorIcmsSub() {
		return valorIcmsSub;
	}

	public void setValorIcmsSub(BigDecimal valorIcmsSub) {
		this.valorIcmsSub = valorIcmsSub;
	}

	public BigDecimal getValorImportacao() {
		return valorImportacao;
	}

	public void setValorImportacao(BigDecimal valorImportacao) {
		this.valorImportacao = valorImportacao;
	}

	public BigDecimal getValorIcmsRemet() {
		return valorIcmsRemet;
	}

	public void setValorIcmsRemet(BigDecimal valorIcmsRemet) {
		this.valorIcmsRemet = valorIcmsRemet;
	}

	public BigDecimal getValorFcp() {
		return valorFcp;
	}

	public void setValorFcp(BigDecimal valorFcp) {
		this.valorFcp = valorFcp;
	}

	public BigDecimal getValorPis() {
		return valorPis;
	}

	public void setValorPis(BigDecimal valorPis) {
		this.valorPis = valorPis;
	}

	public BigDecimal getValorTotalProduto() {
		return valorTotalProduto;
	}

	public void setValorTotalProduto(BigDecimal valorTotalProduto) {
		this.valorTotalProduto = valorTotalProduto;
	}

	public BigDecimal getValorFrete() {
		return valorFrete;
	}

	public void setValorFrete(BigDecimal valorFrete) {
		this.valorFrete = valorFrete;
	}

	public BigDecimal getValorSeguro() {
		return valorSeguro;
	}

	public void setValorSeguro(BigDecimal valorSeguro) {
		this.valorSeguro = valorSeguro;
	}

	public BigDecimal getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(BigDecimal valorDesconto) {
		this.valorDesconto = valorDesconto;
	}

	public BigDecimal getValorOutrasDesp() {
		return valorOutrasDesp;
	}

	public void setValorOutrasDesp(BigDecimal valorOutrasDesp) {
		this.valorOutrasDesp = valorOutrasDesp;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public BigDecimal getValorIcmsDest() {
		return valorIcmsDest;
	}

	public void setValorIcmsDest(BigDecimal valorIcmsDest) {
		this.valorIcmsDest = valorIcmsDest;
	}

	public BigDecimal getValorTotalTrib() {
		return valorTotalTrib;
	}

	public void setValorTotalTrib(BigDecimal valorTotalTrib) {
		this.valorTotalTrib = valorTotalTrib;
	}

	public BigDecimal getValorCofins() {
		return valorCofins;
	}

	public void setValorCofins(BigDecimal valorCofins) {
		this.valorCofins = valorCofins;
	}

	public BigDecimal getValorTotalNota() {
		return valorTotalNota;
	}

	public void setValorTotalNota(BigDecimal valorTotalNota) {
		this.valorTotalNota = valorTotalNota;
	}
}

