package br.com.gestop.administrador.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.administrador.model.Cliente;
import br.com.gestop.administrador.model.filter.ClienteFilter;
import br.com.gestop.administrador.service.ClienteService;
import br.com.gestop.generic.controller.GenericBaseController;

@RestController
@RequestMapping("/gestop-api/cliente")
public class ClienteController extends GenericBaseController<ClienteService, Cliente, Long> {
	
	@GetMapping	
	public Page<Cliente> pequisar(ClienteFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
