package br.com.gestop.administrador.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.administrador.model.Produto;
import br.com.gestop.administrador.model.filter.ProdutoFilter;
import br.com.gestop.administrador.service.ProdutoService;
import br.com.gestop.generic.controller.GenericBaseController;

@RestController
@RequestMapping("/gestop-api/produto")
public class ProdutoController extends GenericBaseController<ProdutoService, Produto, Long> {
	
	@GetMapping	
	public Page<Produto> pequisar(ProdutoFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
