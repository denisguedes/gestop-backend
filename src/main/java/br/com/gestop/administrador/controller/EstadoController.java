package br.com.gestop.administrador.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.administrador.model.Estado;
import br.com.gestop.administrador.model.filter.EstadoFilter;
import br.com.gestop.administrador.service.EstadoService;
import br.com.gestop.generic.controller.GenericBaseController;

@RestController
@RequestMapping("/gestop-api/estado")
public class EstadoController extends GenericBaseController<EstadoService, Estado, Long>{

	@GetMapping	
	public Page<Estado> pequisar(EstadoFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
}
