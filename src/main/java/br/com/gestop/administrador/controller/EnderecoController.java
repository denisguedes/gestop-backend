package br.com.gestop.administrador.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.administrador.model.Endereco;
import br.com.gestop.administrador.model.filter.EnderecoFilter;
import br.com.gestop.administrador.service.EnderecoService;
import br.com.gestop.generic.controller.GenericBaseController;

@RestController
@RequestMapping("/gestop-api/endereco")
public class EnderecoController extends GenericBaseController<EnderecoService, Endereco, Long>{

	@GetMapping	
	public Page<Endereco> pequisar(EnderecoFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
}
