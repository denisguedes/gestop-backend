package br.com.gestop.administrador.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.administrador.model.Fornecedor;
import br.com.gestop.administrador.model.filter.FornecedorFilter;
import br.com.gestop.administrador.service.FornecedorService;
import br.com.gestop.generic.controller.GenericBaseController;

@RestController
@RequestMapping("/gestop-api/fornecedor")
public class FornecedorController extends GenericBaseController<FornecedorService, Fornecedor, Long> {
	
	@GetMapping	
	public Page<Fornecedor> pequisar(FornecedorFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
