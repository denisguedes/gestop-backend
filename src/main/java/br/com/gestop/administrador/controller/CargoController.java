package br.com.gestop.administrador.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.administrador.model.Cargo;
import br.com.gestop.administrador.model.filter.CargoFilter;
import br.com.gestop.administrador.service.CargoService;
import br.com.gestop.generic.controller.GenericBaseController;

@RestController
@RequestMapping("/gestop-api/cargo")
public class CargoController extends GenericBaseController<CargoService, Cargo, Long> {
	
	@GetMapping	
	public Page<Cargo> pequisar(CargoFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
