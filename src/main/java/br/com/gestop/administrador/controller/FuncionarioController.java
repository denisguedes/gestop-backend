package br.com.gestop.administrador.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.administrador.model.Funcionario;
import br.com.gestop.administrador.model.filter.FuncionarioFilter;
import br.com.gestop.administrador.service.FuncionarioService;
import br.com.gestop.generic.controller.GenericBaseController;

@RestController
@RequestMapping("/gestop-api/funcionario")
public class FuncionarioController extends GenericBaseController<FuncionarioService, Funcionario, Long> {
	
	@GetMapping	
	public Page<Funcionario> pequisar(FuncionarioFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
