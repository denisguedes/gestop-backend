package br.com.gestop.administrador.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.administrador.model.Pais;
import br.com.gestop.administrador.model.filter.PaisFilter;
import br.com.gestop.administrador.service.PaisService;
import br.com.gestop.generic.controller.GenericBaseController;

@RestController
@RequestMapping("/gestop-api/pais")
public class PaisController extends GenericBaseController<PaisService, Pais, Long> {
	
	@GetMapping	
	public Page<Pais> pequisar(PaisFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
