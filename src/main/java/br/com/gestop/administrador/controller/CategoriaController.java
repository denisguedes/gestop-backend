package br.com.gestop.administrador.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.administrador.model.Categoria;
import br.com.gestop.administrador.model.filter.CategoriaFilter;
import br.com.gestop.administrador.service.CategoriaService;
import br.com.gestop.generic.controller.GenericBaseController;

@RestController
@RequestMapping("/gestop-api/categoria")
public class CategoriaController extends GenericBaseController<CategoriaService, Categoria, Long> {
	
	@GetMapping	
	public Page<Categoria> pequisar(CategoriaFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
