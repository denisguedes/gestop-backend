package br.com.gestop.administrador.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.administrador.model.Cidade;
import br.com.gestop.administrador.model.filter.CidadeFilter;
import br.com.gestop.administrador.service.CidadeService;
import br.com.gestop.generic.controller.GenericBaseController;

@RestController
@RequestMapping("/gestop-api/cidade")
public class CidadeController extends GenericBaseController<CidadeService, Cidade, Long> {
	
	@GetMapping	
	public Page<Cidade> pequisar(CidadeFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
