package br.com.gestop.administrador.enums;

public enum TipoCliente {

	FISICA("Física"),
	JURIDICA("Jurídica");
	
	private final String descricao;
	
	TipoCliente(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
