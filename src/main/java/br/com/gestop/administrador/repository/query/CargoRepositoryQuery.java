package br.com.gestop.administrador.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.administrador.model.Cargo;
import br.com.gestop.administrador.model.filter.CargoFilter;

public interface CargoRepositoryQuery {

	public Page<Cargo> pequisar(CargoFilter filtro, Pageable pageable);
	
}
