package br.com.gestop.administrador.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.administrador.model.Endereco;
import br.com.gestop.administrador.model.filter.EnderecoFilter;

public interface EnderecoRepositoryQuery {

	public Page<Endereco> pequisar(EnderecoFilter filtro, Pageable pageable);
	
}
