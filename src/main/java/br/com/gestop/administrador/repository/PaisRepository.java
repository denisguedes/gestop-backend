package br.com.gestop.administrador.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gestop.administrador.model.Pais;
import br.com.gestop.administrador.repository.query.PaisRepositoryQuery;

@Repository
public interface PaisRepository extends JpaRepository<Pais, Long>, PaisRepositoryQuery {

}
