package br.com.gestop.administrador.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.administrador.model.Estado;
import br.com.gestop.administrador.model.filter.EstadoFilter;

public interface EstadoRepositoryQuery {

	public Page<Estado> pequisar(EstadoFilter filtro, Pageable pageable);
	
}
