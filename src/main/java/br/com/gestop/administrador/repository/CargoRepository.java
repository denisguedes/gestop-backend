package br.com.gestop.administrador.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gestop.administrador.model.Cargo;
import br.com.gestop.administrador.repository.query.CargoRepositoryQuery;

@Repository
public interface CargoRepository extends JpaRepository<Cargo, Long>, CargoRepositoryQuery {

}
