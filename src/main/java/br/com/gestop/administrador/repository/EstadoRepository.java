package br.com.gestop.administrador.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gestop.administrador.model.Estado;
import br.com.gestop.administrador.repository.query.EstadoRepositoryQuery;

@Repository
public interface EstadoRepository extends JpaRepository<Estado, Long>, EstadoRepositoryQuery {

}
