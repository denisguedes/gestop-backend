package br.com.gestop.administrador.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gestop.administrador.model.Endereco;
import br.com.gestop.administrador.repository.query.EnderecoRepositoryQuery;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Long>, EnderecoRepositoryQuery {

}
