package br.com.gestop.administrador.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.administrador.model.Pais;
import br.com.gestop.administrador.model.filter.PaisFilter;

public interface PaisRepositoryQuery {

	public Page<Pais> pequisar(PaisFilter filtro, Pageable pageable);
	
}
