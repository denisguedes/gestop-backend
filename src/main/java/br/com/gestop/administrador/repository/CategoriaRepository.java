package br.com.gestop.administrador.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gestop.administrador.model.Categoria;
import br.com.gestop.administrador.repository.query.CategoriaRepositoryQuery;

@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Long>, CategoriaRepositoryQuery {

}
