package br.com.gestop.administrador.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.administrador.model.Funcionario;
import br.com.gestop.administrador.model.filter.FuncionarioFilter;

public interface FuncionarioRepositoryQuery {

	public Page<Funcionario> pequisar(FuncionarioFilter filtro, Pageable pageable);
	
}
