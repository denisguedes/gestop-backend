package br.com.gestop.administrador.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.administrador.model.Cliente;
import br.com.gestop.administrador.model.filter.ClienteFilter;

public interface ClienteRepositoryQuery {

	public Page<Cliente> pequisar(ClienteFilter filtro, Pageable pageable);
	
	public Cliente findByNome(String nome);
	
}
