package br.com.gestop.administrador.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.administrador.model.Produto;
import br.com.gestop.administrador.model.filter.ProdutoFilter;

public interface ProdutoRepositoryQuery {

	public Page<Produto> pequisar(ProdutoFilter filtro, Pageable pageable);
	
}
