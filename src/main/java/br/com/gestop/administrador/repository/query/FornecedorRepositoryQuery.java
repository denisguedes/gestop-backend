package br.com.gestop.administrador.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.administrador.model.Fornecedor;
import br.com.gestop.administrador.model.filter.FornecedorFilter;

public interface FornecedorRepositoryQuery {

	public Page<Fornecedor> pequisar(FornecedorFilter filtro, Pageable pageable);
	
}
