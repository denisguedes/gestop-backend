package br.com.gestop.administrador.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gestop.administrador.model.Fornecedor;
import br.com.gestop.administrador.repository.query.FornecedorRepositoryQuery;

@Repository
public interface FornecedorRepository extends JpaRepository<Fornecedor, Long>, FornecedorRepositoryQuery {

}
