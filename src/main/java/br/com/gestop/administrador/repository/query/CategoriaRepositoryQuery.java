package br.com.gestop.administrador.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.administrador.model.Categoria;
import br.com.gestop.administrador.model.filter.CategoriaFilter;

public interface CategoriaRepositoryQuery {

	public Page<Categoria> pequisar(CategoriaFilter filtro, Pageable pageable);
	
}
