package br.com.gestop.administrador.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.administrador.model.Cidade;
import br.com.gestop.administrador.model.filter.CidadeFilter;

public interface CidadeRepositoryQuery {

	public Page<Cidade> pequisar(CidadeFilter filtro, Pageable pageable);
	
}
