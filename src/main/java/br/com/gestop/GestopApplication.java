package br.com.gestop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

import br.com.gestop.seguranca.config.property.ConfigProperties;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(ConfigProperties.class)
public class GestopApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestopApplication.class, args);
	}

}
