package br.com.gestop.financeiro.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.financeiro.model.Pagamento;
import br.com.gestop.financeiro.model.filter.PagamentoFilter;

public interface PagamentoRepositoryQuery {

	public Page<Pagamento> pequisar(PagamentoFilter filtro, Pageable pageable);
	
}
