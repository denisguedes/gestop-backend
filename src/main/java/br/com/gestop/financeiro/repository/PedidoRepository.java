package br.com.gestop.financeiro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gestop.financeiro.model.Pedido;
import br.com.gestop.financeiro.repository.query.PedidoRepositoryQuery;

@Repository
public interface PedidoRepository extends JpaRepository<Pedido, Long>, PedidoRepositoryQuery {

}
