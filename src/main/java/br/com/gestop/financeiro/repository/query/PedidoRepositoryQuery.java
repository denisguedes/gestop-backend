package br.com.gestop.financeiro.repository.query;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.financeiro.enums.Periodo;
import br.com.gestop.financeiro.model.Pedido;
import br.com.gestop.financeiro.model.filter.PedidoFilter;

public interface PedidoRepositoryQuery {

	public Page<Pedido> pequisar(PedidoFilter filtro, Pageable pageable);
	
	public List<Pedido> verificaAgendamento(LocalDate dataInicio, LocalDate dataFim, Periodo periodo);
	
}
