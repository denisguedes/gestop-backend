package br.com.gestop.financeiro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gestop.financeiro.model.Pagamento;
import br.com.gestop.financeiro.repository.query.PagamentoRepositoryQuery;

@Repository
public interface PagamentoRepository extends JpaRepository<Pagamento, Long>, PagamentoRepositoryQuery {

}
