package br.com.gestop.financeiro.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.financeiro.model.FormaPagamento;
import br.com.gestop.financeiro.model.filter.FormaPagamentoFilter;

public interface FormaPagamentoRepositoryQuery {

	public Page<FormaPagamento> pequisar(FormaPagamentoFilter filtro, Pageable pageable);
	
}
