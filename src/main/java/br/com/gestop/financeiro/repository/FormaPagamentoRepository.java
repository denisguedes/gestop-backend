package br.com.gestop.financeiro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gestop.financeiro.model.FormaPagamento;
import br.com.gestop.financeiro.repository.query.FormaPagamentoRepositoryQuery;

@Repository
public interface FormaPagamentoRepository extends JpaRepository<FormaPagamento, Long>, FormaPagamentoRepositoryQuery {

}
