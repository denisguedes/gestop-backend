package br.com.gestop.financeiro.repository.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import br.com.gestop.financeiro.enums.Periodo;
import br.com.gestop.financeiro.model.Pedido;
import br.com.gestop.financeiro.model.filter.PedidoFilter;
import br.com.gestop.financeiro.repository.query.PedidoRepositoryQuery;

public class PedidoRepositoryImpl implements PedidoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Pedido> pequisar(PedidoFilter filtro, Pageable pageable) {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Pedido> criteria = builder.createQuery(Pedido.class);
		Root<Pedido> root = criteria.from(Pedido.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);
		criteria.orderBy(builder.desc(root.get("dataPedido")));

		TypedQuery<Pedido> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);

		return new PageImpl<>(query.getResultList(), pageable, total(filtro));
	}

	private Predicate[] criarRestricoes(PedidoFilter filtro, CriteriaBuilder builder,
			Root<Pedido> root) {

		List<Predicate> predicates = new ArrayList<>();

		if(filtro.getCodigo() != null) {			
			predicates.add(builder.equal(root.get("codigo"), filtro.getCodigo()));
		}
		
		if(!StringUtils.isEmpty(filtro.getNome())) {
			predicates.add(builder.equal(root.get("cliente"), filtro.getCliente()));
		}
		
		if (!StringUtils.isEmpty(filtro.getDataPedido())) {
			predicates.add(builder.like(builder.lower(root.get("dataPedido")),
					"%" + filtro.getDataPedido() + "%"));
		}				

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;

		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);

	}

	private Long total(PedidoFilter filtro) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Pedido> root = criteria.from(Pedido.class);

		Predicate[] predicates = criarRestricoes(filtro, builder, root);
		criteria.where(predicates);

		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

	@Override
	public List<Pedido> verificaAgendamento(LocalDate dataInicio, LocalDate dataFim, Periodo periodo) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Pedido> criteria = builder.createQuery(Pedido.class);
		Root<Pedido> root = criteria.from(Pedido.class);
		
			criteria.where(builder.or(
					builder.between(root.get("dataInicio"), dataInicio, dataFim),
					builder.between(root.get("dataFim"), dataInicio, dataFim)),
					builder.equal(root.get("periodo"), periodo));
		criteria.orderBy(builder.desc(root.get("dataPedido")));

		TypedQuery<Pedido> query = manager.createQuery(criteria);
		
		return query.getResultList();
	}
	
}
