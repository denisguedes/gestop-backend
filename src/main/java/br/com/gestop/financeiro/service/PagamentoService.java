package br.com.gestop.financeiro.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.financeiro.enums.statusPagamento;
import br.com.gestop.financeiro.model.FormaPagamento;
import br.com.gestop.financeiro.model.Pagamento;
import br.com.gestop.financeiro.model.filter.PagamentoFilter;
import br.com.gestop.financeiro.repository.PagamentoRepository;
import br.com.gestop.generic.service.GenericBaseService;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class PagamentoService extends GenericBaseService<PagamentoRepository, Pagamento, Long>{
	
	@Autowired
	private FormaPagamentoService formaPagamentoService;
	
	public Page<Pagamento> pequisar(PagamentoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public List<Pagamento> parcelamento(Long codigo, BigDecimal valor, Long numero, LocalDate dataVencimento){
		BigDecimal valorParcela = valor.divide(BigDecimal.valueOf(numero));
		List<Pagamento> pagamentos = new ArrayList<>();
		FormaPagamento formaPagamento = formaPagamentoService.buscarPorCodigo(codigo);
		
		for(int i = 1; i <= numero; i++) {
			Pagamento pagamento = new Pagamento();
			pagamento.setValorParcela(valorParcela);
	        pagamento.setValorPagamento(valor);
	        pagamento.setDataVencimento(dataVencimento);
	        pagamento.setFormaPagamento(formaPagamento);
	        if(i > 1) {
		        pagamento.setDataVencimento(dataVencimento.plusMonths(i-1));
	        }
	        pagamento.setNumero(i);
	        pagamento.setStatus(statusPagamento.PENDENTE);
	        pagamentos.add(pagamento);
		}
		return pagamentos;
	}
	
	public byte[] relatorioContasPagar(LocalDate inicio, LocalDate fim) throws Exception {
//		List<LancamentoEstatisticaPessoa> dados = lancamentoRepository.porPessoa(inicio, fim);
		
		Map<String, Object> parametros = new HashMap<>();
		parametros.put("DT_INICIO", Date.valueOf(inicio));
		parametros.put("DT_FIM", Date.valueOf(fim));
		parametros.put("REPORT_LOCALE", new Locale("pt", "BR"));
		
		InputStream inputStream = this.getClass().getResourceAsStream(
				"/relatorios/lancamentos-por-pessoa.jasper");
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, parametros,
				new JRBeanCollectionDataSource(null));
		
		return JasperExportManager.exportReportToPdf(jasperPrint);
	}
}
