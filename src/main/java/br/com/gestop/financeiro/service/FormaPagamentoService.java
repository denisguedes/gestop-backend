package br.com.gestop.financeiro.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.financeiro.model.FormaPagamento;
import br.com.gestop.financeiro.model.filter.FormaPagamentoFilter;
import br.com.gestop.financeiro.repository.FormaPagamentoRepository;
import br.com.gestop.generic.service.GenericBaseService;

@Service
public class FormaPagamentoService extends GenericBaseService<FormaPagamentoRepository, FormaPagamento, Long>{
	
	public Page<FormaPagamento> pequisar(FormaPagamentoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
}
