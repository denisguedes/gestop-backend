package br.com.gestop.financeiro.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.financeiro.enums.Periodo;
import br.com.gestop.financeiro.model.Pedido;
import br.com.gestop.financeiro.model.filter.PedidoFilter;
import br.com.gestop.financeiro.repository.PedidoRepository;
import br.com.gestop.generic.service.GenericBaseService;

@Service
public class PedidoService extends GenericBaseService<PedidoRepository, Pedido, Long>{
	
	public Page<Pedido> pequisar(PedidoFilter filtro, Pageable pageable) {
		return repositorio.pequisar(filtro, pageable);
	}
	
	public List<Pedido> verificaAgendamento(LocalDate dataInicio, LocalDate dataFim, Periodo periodo) {
		return repositorio.verificaAgendamento(dataInicio, dataFim, periodo);
	}
}
