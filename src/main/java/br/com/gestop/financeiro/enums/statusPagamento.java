package br.com.gestop.financeiro.enums;

public enum statusPagamento {

	PENDENTE("Pendente"),
	CANCELADO("Cancelado"),
	PAGO("Pago");
	
	private final String descricao;
	
	statusPagamento(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
