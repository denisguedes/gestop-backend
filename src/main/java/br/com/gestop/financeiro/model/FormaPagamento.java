package br.com.gestop.financeiro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table(name = "tb_forma_pagamento", schema="bd_fin")
public class FormaPagamento extends GenericBaseModel<Long>{

	@NotNull
	@Size(min = 0, max = 30)
	@Column(name = "descricao")
	private String descricao;
	
	public FormaPagamento() {}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public String toString() {
		return new StringBuilder().append(descricao).toString();
	}
}
