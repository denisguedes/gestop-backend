package br.com.gestop.financeiro.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.gestop.administrador.model.Produto;
import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table(name = "tb_pedido_item", schema = "bd_fin")
public class PedidoItem extends GenericBaseModel<Long> {
	
	@ManyToOne
	@JsonBackReference("pedidosItem")
	@JoinColumn(name = "codigo_pedido")
	private Pedido pedido;
	
	@ManyToOne
	@JoinColumn(name = "codigo_produto")
	private Produto produto;
	
	@Column(name = "quantidade_produto")
	private Integer qtdProduto;
	
	@Column(name = "valor_produto")
	private BigDecimal valorProduto;
	
	public PedidoItem() {
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
	public Integer getQtdProduto() {
		return qtdProduto;
	}

	public void setQtdProduto(Integer qtdProduto) {
		this.qtdProduto = qtdProduto;
	}

	public BigDecimal getValorProduto() {
		return valorProduto;
	}

	public void setValorProduto(BigDecimal valorProduto) {
		this.valorProduto = valorProduto;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if(produto != null) {
			sb.append(produto.getDescricao());
		}
		if(pedido != null) {
			sb.append(" - ").append(pedido.getDataPedido())
							.append("-").append(pedido.getValorPedido());
		}
		return sb.toString();
	}
}
