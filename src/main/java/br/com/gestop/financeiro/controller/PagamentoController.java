package br.com.gestop.financeiro.controller;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.financeiro.model.Pagamento;
import br.com.gestop.financeiro.model.filter.PagamentoFilter;
import br.com.gestop.financeiro.service.PagamentoService;
import br.com.gestop.generic.controller.GenericBaseController;

@RestController
@RequestMapping("/gestop-api/pagamento")
public class PagamentoController extends GenericBaseController<PagamentoService, Pagamento, Long> {
	
	@GetMapping	
	public Page<Pagamento> pequisar(PagamentoFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
	@GetMapping("/relatorio")
	public void gerarRelatorioPagamento(@PathVariable Long codigo) {
		System.out.println("RELATÓRIO");
	}
	
	@GetMapping("/parcelamento")
	public List<Pagamento> parcelamentoPagamento(@RequestParam Long codigo, @RequestParam BigDecimal valor, 
												 @RequestParam Long numero, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataVencimento){
		return servico.parcelamento(codigo, valor, numero, dataVencimento);
	}
	
	public ResponseEntity<Pagamento> parcelamento(@RequestBody Pagamento pagamento){
		System.out.println("TESTE...");
		return null;
	}
	
}
