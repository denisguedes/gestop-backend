package br.com.gestop.financeiro.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.financeiro.model.FormaPagamento;
import br.com.gestop.financeiro.model.filter.FormaPagamentoFilter;
import br.com.gestop.financeiro.service.FormaPagamentoService;
import br.com.gestop.generic.controller.GenericBaseController;

@RestController
@RequestMapping("/gestop-api/forma-pagamento")
public class FormaPagamentoController extends GenericBaseController<FormaPagamentoService, FormaPagamento, Long> {
	
	@GetMapping	
	public Page<FormaPagamento> pequisar(FormaPagamentoFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
