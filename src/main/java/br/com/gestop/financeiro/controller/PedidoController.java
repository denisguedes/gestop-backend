package br.com.gestop.financeiro.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.administrador.service.ClienteService;
import br.com.gestop.financeiro.enums.Periodo;
import br.com.gestop.financeiro.model.Pedido;
import br.com.gestop.financeiro.model.filter.PedidoFilter;
import br.com.gestop.financeiro.service.PedidoService;
import br.com.gestop.generic.controller.GenericBaseController;

@RestController
@RequestMapping("/gestop-api/pedido")
public class PedidoController extends GenericBaseController<PedidoService, Pedido, Long> {
	
	@Autowired
	private ClienteService clienteService;
	
	@GetMapping	
	public Page<Pedido> pequisar(PedidoFilter filtro, Pageable pageable) {
		if(!StringUtils.isEmpty(filtro.getNome())) {
			filtro.setCliente(clienteService.buscarPorNome(filtro.getNome()));
		}
		return servico.pequisar(filtro, pageable);
	}
	
	@GetMapping("/agendamento")
	public List<Pedido> verificarAgendamento(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataInicio, 
			@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dataFim, 
			@RequestParam Periodo periodo) {
		return servico.verificaAgendamento(dataInicio, dataFim, periodo);
	}
	
}
