package br.com.gestop.seguranca.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.generic.controller.GenericBaseController;
import br.com.gestop.seguranca.model.Usuario;
import br.com.gestop.seguranca.model.filter.UsuarioFilter;
import br.com.gestop.seguranca.service.UsuarioService;

@RestController
@RequestMapping("/gestop-api/usuario")
public class UsuarioController extends GenericBaseController<UsuarioService, Usuario, Long> {
	
	@GetMapping	
	public Page<Usuario> pequisar(UsuarioFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
}