package br.com.gestop.seguranca.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.generic.controller.GenericBaseController;
import br.com.gestop.seguranca.model.Permissao;
import br.com.gestop.seguranca.model.filter.PermissaoFilter;
import br.com.gestop.seguranca.service.PermissaoService;

@RestController
@RequestMapping("/gestop-api/permissao")
public class PermissaoController extends GenericBaseController<PermissaoService, Permissao, Long> {
	
	@GetMapping	
	public Page<Permissao> pequisar(PermissaoFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
