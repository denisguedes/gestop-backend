package br.com.gestop.seguranca.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gestop.generic.controller.GenericBaseController;
import br.com.gestop.seguranca.model.Perfil;
import br.com.gestop.seguranca.model.filter.PerfilFilter;
import br.com.gestop.seguranca.service.PerfilService;

@RestController
@RequestMapping("/gestop-api/perfil")
public class PerfilController extends GenericBaseController<PerfilService, Perfil, Long> {
	
	@GetMapping	
	public Page<Perfil> pequisar(PerfilFilter filtro, Pageable pageable) {
		return servico.pequisar(filtro, pageable);
	}
	
}
