package br.com.gestop.seguranca.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.generic.service.GenericBaseService;
import br.com.gestop.seguranca.model.Usuario;
import br.com.gestop.seguranca.model.filter.UsuarioFilter;
import br.com.gestop.seguranca.other.CriptografaSenha;
import br.com.gestop.seguranca.repository.UsuarioRepository;

@Service
public class UsuarioService extends GenericBaseService<UsuarioRepository, Usuario, Long>{	
	
	public Page<Usuario> pequisar(UsuarioFilter usuarioFilter, Pageable pageable) {
		return repositorio.pequisar(usuarioFilter, pageable);
	}
	
	@Override
	public Usuario incluir(Usuario entidade) {
		CriptografaSenha geradorSenha = new CriptografaSenha();
		entidade.setSenha(geradorSenha.codificarSenha(entidade.getSenha()));
		return super.incluir(entidade);
	}
	
	@Override
	public Usuario alterar(Long id, Usuario entidade) {
		
		Usuario usuario = buscarPorCodigo(id);
		
		if(usuario.getSenha().equals(entidade.getSenha())) {
			return super.alterar(id, entidade);
		}
		
		CriptografaSenha geradorSenha = new CriptografaSenha();
		entidade.setSenha(geradorSenha.codificarSenha(entidade.getSenha()));
		return super.alterar(id, entidade);
	}
	
	public Optional<Usuario> buscarPorEmail(String email){
		Optional<Usuario> usuarioOptional = repositorio.findByEmail(email);
		return usuarioOptional;
	}

}
