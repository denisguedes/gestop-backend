package br.com.gestop.seguranca.service;

import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.generic.service.GenericBaseService;
import br.com.gestop.seguranca.model.Perfil;
import br.com.gestop.seguranca.model.filter.PerfilFilter;
import br.com.gestop.seguranca.repository.PerfilRepository;

@Service
public class PerfilService  extends GenericBaseService<PerfilRepository, Perfil, Long>{
	
	public Page<Perfil> pequisar(PerfilFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
	
	public Perfil alterar(Long id, Perfil entidade) {
		Perfil entidadeSalva = buscarPorCodigo(id);
		
		entidadeSalva.getPermissoes().clear();
		entidadeSalva.getPermissoes().addAll(entidade.getPermissoes());		
		
		BeanUtils.copyProperties(entidade, entidadeSalva, "codigo", "permissoes");		
		return repositorio.save(entidadeSalva);
	}
	
}
