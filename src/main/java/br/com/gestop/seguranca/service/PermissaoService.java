package br.com.gestop.seguranca.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.gestop.generic.service.GenericBaseService;
import br.com.gestop.seguranca.model.Permissao;
import br.com.gestop.seguranca.model.filter.PermissaoFilter;
import br.com.gestop.seguranca.repository.PermissaoRepository;

@Service
public class PermissaoService  extends GenericBaseService<PermissaoRepository, Permissao, Long>{
	
	public Page<Permissao> pequisar(PermissaoFilter Filtro, Pageable pageable) {
		return repositorio.pequisar(Filtro, pageable);
	}
}
