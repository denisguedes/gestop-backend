package br.com.gestop.seguranca.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table(name = "tb_perfil_permissao", schema = "bdadm")
public class PerfilPermissao extends GenericBaseModel<Long> {

	@JsonBackReference("permissoes")
	@ManyToOne
	@JoinColumn(name = "id_perfil")
	private Perfil perfil;

	@ManyToOne
	@JoinColumn(name = "id_permissao")
	private Permissao permissao;

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Permissao getPermissao() {
		return permissao;
	}

	public void setPermissao(Permissao permissao) {
		this.permissao = permissao;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if(perfil != null) {
			sb.append(perfil.getDescricao());
		}
		if(permissao != null) {
			sb.append(" - ").append(permissao.getRole());
		}
		return sb.toString();
	}
}
