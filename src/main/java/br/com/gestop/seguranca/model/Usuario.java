
package br.com.gestop.seguranca.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table(name = "tb_usuario", schema = "bdadm")
public class Usuario extends GenericBaseModel<Long> {

	@NotNull
	@Size(min = 0, max = 30)
	@Column(name = "login")
	private String login;
	
	@NotNull
	@Email
	@Size(min = 0, max = 100)
	@Column(name = "email")
	private String email;

	@NotNull
	@Size(min = 0, max = 100)
	@Column(name = "senha")
	private String senha;

	@JsonManagedReference("permissoes")
	@OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private List<UsuarioPermissao> permissoes;

	public Usuario() {
		permissoes = new ArrayList<>();
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public List<UsuarioPermissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(List<UsuarioPermissao> permissoes) {
		if(permissoes != null) {
			this.permissoes.clear();
			this.permissoes.addAll(permissoes);
		}else {
			this.permissoes = permissoes;
		}
	}

	@Override
	public String toString() {
		return new StringBuilder().append(login).toString();
	}
}
