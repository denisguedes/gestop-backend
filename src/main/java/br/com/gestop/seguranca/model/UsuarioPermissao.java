package br.com.gestop.seguranca.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table(name = "tb_usuario_permissao", schema = "bdadm")
public class UsuarioPermissao extends GenericBaseModel<Long> {

	@ManyToOne
	@JsonBackReference("permissoes")
	@JoinColumn(name = "codigo_usuario")
	private Usuario usuario;

	@ManyToOne
	@JoinColumn(name = "codigo_permissao")
	private Permissao permissao;
	
	public UsuarioPermissao() {
		this.usuario = new Usuario();
		this.permissao = new Permissao();
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Permissao getPermissao() {
		return permissao;
	}

	public void setPermissao(Permissao permissao) {
		this.permissao = permissao;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if(usuario != null) {
			sb.append(usuario.getLogin());
		}else if(permissao != null) {
			sb.append(" - ").append(permissao.getDescricao());
		}
		return sb.toString();
	}
}
