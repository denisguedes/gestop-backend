package br.com.gestop.seguranca.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table( name="tb_perfil", schema = "bdadm")
public class Perfil extends GenericBaseModel<Long>{

	@NotNull
	@Size(min = 3, max = 30)
	@Column(name = "descricao")
	private String descricao;
	
	@JsonManagedReference("permissoes")
	@OneToMany(mappedBy = "perfil", cascade = CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)	
	private List<PerfilPermissao> permissoes = new ArrayList<PerfilPermissao>();

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<PerfilPermissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(List<PerfilPermissao> permissoes) {
		if(permissoes != null) {
			this.permissoes.clear();
			this.permissoes.addAll(permissoes);
		}else {
			this.permissoes = permissoes;
		}
	}	
		
}
