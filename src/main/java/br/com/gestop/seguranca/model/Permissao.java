package br.com.gestop.seguranca.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.gestop.generic.model.GenericBaseModel;

@Entity
@Table(name = "tb_permissao", schema = "bdadm")
public class Permissao extends GenericBaseModel<Long> {

	@NotNull
	@Size(min = 3, max = 50)
	@Column(name = "descricao")
	private String descricao;

	@NotNull
	@Size(min = 3, max = 50)
	@Column(name = "role")
	private String role;
	
	public Permissao() {}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return new StringBuilder().append(descricao).append(" - ").append(role).toString();
	}
}
