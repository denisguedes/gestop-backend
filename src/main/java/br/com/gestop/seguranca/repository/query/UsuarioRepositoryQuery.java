package br.com.gestop.seguranca.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.seguranca.model.Usuario;
import br.com.gestop.seguranca.model.filter.UsuarioFilter;

public interface UsuarioRepositoryQuery {

	public Page<Usuario> pequisar(UsuarioFilter usuarioFilter, Pageable pageable);
	
}
