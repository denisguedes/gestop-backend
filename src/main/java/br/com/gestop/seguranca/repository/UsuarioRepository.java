package br.com.gestop.seguranca.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gestop.seguranca.model.Usuario;
import br.com.gestop.seguranca.repository.query.UsuarioRepositoryQuery;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>, UsuarioRepositoryQuery {

	public Optional<Usuario> findByEmail(String email);
	
	public Optional<Usuario> findByLoginOrEmail(String login, String email);
	
}