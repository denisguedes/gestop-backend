package br.com.gestop.seguranca.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gestop.seguranca.model.Permissao;
import br.com.gestop.seguranca.repository.query.PermissaoRepositoryQuery;

@Repository
public interface PermissaoRepository extends JpaRepository<Permissao, Long>, PermissaoRepositoryQuery {

}
