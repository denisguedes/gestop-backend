package br.com.gestop.seguranca.repository.query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.gestop.seguranca.model.Permissao;
import br.com.gestop.seguranca.model.filter.PermissaoFilter;

public interface PermissaoRepositoryQuery {

	public Page<Permissao> pequisar(PermissaoFilter filtro, Pageable pageable);
	
}
