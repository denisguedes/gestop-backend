package br.com.gestop.seguranca.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.gestop.seguranca.model.Perfil;
import br.com.gestop.seguranca.repository.query.PerfilRepositoryQuery;

@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Long>, PerfilRepositoryQuery {

}
