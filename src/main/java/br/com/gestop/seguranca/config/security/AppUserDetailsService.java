package br.com.gestop.seguranca.config.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.gestop.seguranca.model.Permissao;
import br.com.gestop.seguranca.model.Usuario;
import br.com.gestop.seguranca.model.UsuarioPermissao;
import br.com.gestop.seguranca.repository.PermissaoRepository;
import br.com.gestop.seguranca.repository.UsuarioRepository;

@Service
public class AppUserDetailsService implements UserDetailsService {
		
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private PermissaoRepository permissaoRepository;
	
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		
		if(login.equals("administrador")) {
			return usuarioMestre(login);
		}
				
		Optional<Usuario> usuarioOptional = usuarioRepository.findByLoginOrEmail(login, login);
		Usuario usuario = usuarioOptional.orElseThrow(() -> new UsernameNotFoundException("Usuário e/ou senha incorretos"));
		return new UsuarioSistema(usuario, getPermissoes(usuario));
	}

	private Collection<? extends GrantedAuthority> getPermissoes(Usuario usuario) {
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		
		List<UsuarioPermissao> permissoesUsuario = usuario.getPermissoes() != null ? usuario.getPermissoes() : new ArrayList<>();
		permissoesUsuario.forEach(p -> authorities.add(new SimpleGrantedAuthority(p.getPermissao().getRole().toUpperCase())));
				
		return authorities;
	}
	
	private UserDetails usuarioMestre(String email) throws UsernameNotFoundException {
		
		Usuario usuario = new Usuario();
		usuario.setEmail("administrador");
		usuario.setSenha("$2a$10$rmaoICl3ur4AAxI2TACAH.8tKT3ZcSwe/7ha9hbe0.suwj.r7C0q2"); //mestre@admin
		
		return new UsuarioSistema(usuario, getAllPermissoes());
	}
	
	private Collection<? extends GrantedAuthority> getAllPermissoes() {
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		List<Permissao> permissoes = permissaoRepository.findAll();
		permissoes.forEach(p -> authorities.add(new SimpleGrantedAuthority(p.getRole().toUpperCase())));		
		return authorities;
	}
	
}
