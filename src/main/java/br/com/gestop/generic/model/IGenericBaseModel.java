package br.com.gestop.generic.model;

import java.io.Serializable;
import java.time.LocalDate;

public interface IGenericBaseModel {
	
	Serializable getCodigo();
	
	LocalDate getDataCadastro();
	
	LocalDate getDataAlteracao();
	
}
