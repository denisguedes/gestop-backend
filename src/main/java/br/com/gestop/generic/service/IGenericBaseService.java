package br.com.gestop.generic.service;

import br.com.gestop.generic.model.IGenericBaseModel;

public interface IGenericBaseService<E extends IGenericBaseModel, T> {

	E incluir(E entidade);
	E alterar(Long id, E entidade);
	void excluir(T id);
	E buscarPorCodigo(T id);
	Object listar();
		
}
