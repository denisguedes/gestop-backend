package br.com.gestop.generic.controller;


import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import br.com.gestop.generic.model.IGenericBaseModel;
import br.com.gestop.generic.service.IGenericBaseService;

/*
* @param <S> Classe do serviço da classe a ser controlada.
* @param <E> Classe da entidade a ser manipulada.
* @param <T> Classe do tipo de dado do id da classe a ser manipulada.
*/

public class GenericBaseController<S extends IGenericBaseService<E, T>, E extends IGenericBaseModel, T> {

	@Autowired
	protected S servico;

//	@Autowired
//	private ApplicationEventPublisher publisher;

	// Novo
	@PostMapping
	public ResponseEntity<?> incluir(@Valid @RequestBody E entidade, HttpServletResponse response) {

		E novo = servico.incluir(entidade);
	//	publisher.publishEvent(new RecursoCriadoEvent(this, response, (Long) novo.getCodigo()));
		return ResponseEntity.status(HttpStatus.CREATED).body(novo);

	}

	// Editar
	@PutMapping("/{id}")
	public ResponseEntity<?> alterar(@PathVariable Long id, @Valid @RequestBody E entidade) {
		E entidadeSalva = servico.alterar(id, entidade);
		return ResponseEntity.ok(entidadeSalva);
	}

	// Remover
	@DeleteMapping("/{id}")
	public void excluir(@PathVariable T id) {
		servico.excluir(id);		
	}

	//Buscar por código
	@GetMapping("/{id}")
	public ResponseEntity<?> buscarPorId(@PathVariable T id) throws Exception {
		E entidade = servico.buscarPorCodigo(id);
		if (entidade == null) {
			return ResponseEntity.notFound().build();
		}
		ResponseEntity<?> response = ResponseEntity.ok(entidade);
		return response;
	}
	
	//Listar
	@GetMapping("/listar")
	public ResponseEntity<?> listar(){
		return ResponseEntity.ok(servico.listar());
	}

}

