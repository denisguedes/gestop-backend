package br.com.gestop.util;

import java.io.ByteArrayOutputStream;
import java.util.Collection;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

public class Report {
	
	private static final String PDF = "pdf";
	private static final String DOC = "doc";
	private static final String JASPER = ".jasper";
	private SimpleOutputStreamExporterOutput exporterOutput;
	private SimpleExporterInput exporterInput;

	public byte[] report(Collection<?> list, Map<String,Object> parameters, String relatorio, String modelo) throws JRException {

		ByteArrayOutputStream byteArray = new ByteArrayOutputStream(); 
		JasperPrint jasperPrint = jasperPrint(list, parameters, relatorio + JASPER);

		exporterOutput = new SimpleOutputStreamExporterOutput(byteArray);
		exporterInput = new SimpleExporterInput(jasperPrint);
		
		if(modelo.equals(PDF)){
			reportPdf();
		}else if(modelo.equals(DOC)){
			reportDoc();
		}else{
			 reportXls();
		}
		return byteArray.toByteArray();
	}
	
	public void reportPdf() throws JRException{
		JRPdfExporter exporterPdf = new JRPdfExporter();
		exporterPdf.setExporterInput(exporterInput);
		exporterPdf.setExporterOutput(exporterOutput);  
		exporterPdf.exportReport(); 
	}
	
	public void reportDoc() throws JRException{
		JROdtExporter exporterDocx = new JROdtExporter();
		exporterDocx.setExporterInput(exporterInput);
		exporterDocx.setExporterOutput(exporterOutput); 
		exporterDocx.exportReport();
	}
	
	public void reportXls() throws JRException{
		JRXlsxExporter exporterXls = new JRXlsxExporter(); 
		exporterXls.setExporterInput(exporterInput);
		exporterXls.setExporterOutput(exporterOutput);
		exporterXls.exportReport();   
	}
	
	public JasperPrint jasperPrint(Collection<?> list, Map<String,Object> parameters, String relatorio) throws JRException {
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(relatorio));
		return JasperFillManager.fillReport(jasperReport, parameters, new JRBeanCollectionDataSource(list));
	}
}
